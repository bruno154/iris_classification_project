from flask import Flask, request, jsonify
import pandas as pd
import pickle
import json

app = Flask(__name__)

<<<<<<< HEAD
#Load preprocessing
scaler_file = open('../artifacts/scaler.pkl', 'rb')
SCALER = pickle.load(scaler_file)

#Loading label encoder
le_file = open('../artifacts/label_encoder.pkl', 'rb')
LE = pickle.load(le_file)

#Load model
model_file = open('../artifacts/model.pkl', 'rb')
MODEL = pickle.load(model_file)
HTTP_BAD_REQUEST = 400

@app.route('/predict', methods=['POST'])
def predict():

    #Getting the post
    jsonfile = request.get_json()

    #checking if we have some data
    if not jsonfile:
        return {
            'error': "Body is empty"
        }, 500

    #Transforming in DataFrame
    data =pd.read_json(json.dumps(jsonfile), orient='index')
    data = data.transpose()
    
    #Applying the standart scaler
    scaled_features = SCALER.transform(data)

    #Predicting
=======
# Load preprocessing
with open("../artifacts/scaler.pkl", "rb") as scaler_file:
    SCALER = pickle.load(scaler_file)

# Loading label encoder
with open("../artifacts/label_encoder.pkl", "rb") as le_file:
    LE = pickle.load(le_file)

# Load model
with open("../artifacts/model.pkl", "rb") as model_file:
    MODEL = pickle.load(model_file)

HTTP_BAD_REQUEST = 400


@app.route("/predict", methods=["POST"])
def predict():
    # Getting the post
    jsonfile = request.get_json()

    # checking if we have some data
    if not jsonfile:
        return {"error": "Body is empty"}, 500

    # Transforming in DataFrame
    data = pd.read_json(json.dumps(jsonfile), orient="index")
    data = data.transpose()

    # Applying the standart scaler
    scaled_features = SCALER.transform(data)

    # Predicting
>>>>>>> 4884407647b588c5e2b8a4b93fcf5d3cac205b54
    try:
        pred = MODEL.predict(scaled_features)
        pred = LE.inverse_transform(pred)
    except Exception as err:
<<<<<<< HEAD
        message = (f'Failed to score the model. Exception: {err}')
        response = jsonify(status='error', error_message=message)
        response.status_code = HTTP_BAD_REQUEST
        return response

    #prettifying the predictions
    r = dict()
    for i in range(len(pred)):
        r[f"Sample-{i}"] = pred[i] 

    return jsonify(r)

if __name__=='__main__':
=======
        message = f"Failed to score the model. Exception: {err}"
        response = jsonify(status="error", error_message=message)
        response.status_code = HTTP_BAD_REQUEST
        return response

    # prettifying the predictions
    r = {}
    for i in range(len(pred)):
        r[f"Sample-{i}"] = pred[i]

    return jsonify(r)


if __name__ == "__main__":
>>>>>>> 4884407647b588c5e2b8a4b93fcf5d3cac205b54
    app.run(debug=True)
