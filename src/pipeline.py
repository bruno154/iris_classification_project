from sklearn.preprocessing import LabelEncoder, StandardScaler
import os
import csv
import pickle
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import yaml
from hyperopt import STATUS_OK, Trials, fmin, hp, tpe
from sklearn import metrics
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder, StandardScaler
from ucimlrepo import fetch_ucirepo


def get_data() -> pd.DataFrame:
    """

    Collects the data from UCI repository.
    https://archive.ics.uci.edu/dataset/53/iris
    """

    # fetch dataset
    iris = fetch_ucirepo(id=53)

    # data (as pandas dataframes)
    X = iris.data.features
    y = iris.data.targets

    # variable information
    print(iris.variables)

    return {"X": X, "y": y}


def split_data(X: pd.DataFrame = None, 
                      y: pd.DataFrame = None
                      ) -> dict:
    """

    Splits the data between train and test.

    X: Pandas dataframe with the features
    y: Padas dataframe with the label variable
    """

    if X is None or y is None:
        raise Exception("Por favor adicionar dados ao Pipeline")
    else:
        X_train, X_test, y_train, y_test = train_test_split(
            X, y, test_size=0.20, random_state=42
        )

    return {"X_train": X_train, "X_test": X_test, "y_train": y_train, "y_test": y_test}


def transform(
    X_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_train: pd.DataFrame,
    y_test: pd.DataFrame,
) -> dict:
    """

    Transform the data in order to be served to the model.

    X_train: A dataframe with training features.
    X_test:  A dataframe with testing features.
    y_train: A dataframe with training label.
    y_test:  A dataframe with testing label.
    """
    le = LabelEncoder()

    # Encoding training labels
    le.fit(y_train)
    y_train = le.transform(y_train)
    y_train = pd.DataFrame(y_train.flatten(), columns=["Label"])

    # Encoding testing labels
    le.fit(y_test)
    y_test = le.transform(y_test)
    y_test = pd.DataFrame(y_test.flatten(), columns=["Label"])

    # Saving pickle encoder
    with open("../artifacts/label_encoder.pkl", "wb") as f:
        pickle.dump(le, f)

    # scalling
    scaler = StandardScaler()

    # Transforming training data
    scaler.fit(X_train)
    colunas = list(X_train.columns.values)
    X_train = scaler.transform(X_train)
    X_train = pd.DataFrame(X_train, columns=colunas)

    # Transforming test data
    scaler.fit(X_test)
    colunas = list(X_test.columns.values)
    X_test = scaler.transform(X_test)
    X_test = pd.DataFrame(X_test, columns=colunas)

    # Saving pickle do scaler
    with open("../artifacts/scaler.pkl", "wb") as f:
        pickle.dump(scaler, f)

    # Putting together
    data_train = pd.concat([X_train, y_train], axis=1)
    data_test = pd.concat([X_test, y_test], axis=1)

    return {"data_train": data_train, "data_test": data_test}


def evaluate(y_test: pd.DataFrame, preds: np.array):
    """

    Auxiliar function to calculate f1 to the hypertunning process.

    y_test: A dataframe with testing label.
    preds:  Model predictions.
    """

    f1 = f1_score(y_test, preds, average="weighted")

    return f1


def hypertunning(
    X_train: pd.DataFrame,
    y_train: pd.DataFrame,
    X_test: pd.DataFrame,
    y_test: pd.DataFrame,
) -> dict:
    # Loading Search Space
    with open("../project.yaml", "rb") as f:
        params = yaml.safe_load(f)

    def objective(params):
        modelo = Perceptron(**params)
        modelo.fit(X_train, y_train)

        pred = modelo.predict(X_test)

        f1 = evaluate(y_test, pred)

        return {"loss": f1, "status": STATUS_OK}

    search_space = {
        "max_iter": params["parameters"]["max_iter"],
        "eta0": hp.uniform(
            "eta0",
            params["hypertunning"]["min_eta0"],
            params["hypertunning"]["max_eta0"],
        ),
    }

    best_result = fmin(
        fn=objective,
        space=search_space,
        algo=tpe.suggest,
        max_evals=10,
        trials=Trials(),
    )

    return best_result


def training(data_train: pd.DataFrame, data_test: pd.DataFrame) -> dict:
    """

    The training function.

    data_train: A dataframe with training data.
    data_test:  A dataframe with testing data.
    """

    # Carregando project yaml
    with open("../project.yaml", "rb") as f:
        params = yaml.safe_load(f)

    # Spliting features and labels

    # Train
    X_train = data_train.iloc[:, :-1]
    y_train = data_train.iloc[:, -1]
    y_train = y_train.astype("int")

    # Test
    X_test = data_test.iloc[:, :-1]
    y_test = data_test.iloc[:, -1]
    y_test = y_test.astype("int")

    if params["hypertunning"]["label"].lower() == "false":
        # Training the modelo
        print("Treinando modelo")
        modelo = Perceptron(
            max_iter=params["parameters"]["max_iter"],
            eta0=params["parameters"]["eta0"],
            random_state=42,
        )
        modelo.fit(X_train, y_train)
        f.close()

        # Saving Model
        with open("../artifacts/model.pkl", "wb") as file:
            pickle.dump(modelo, file)

    else:
        best_result = hypertunning(
            X_train=X_train, y_train=y_train, X_test=X_test, y_test=y_test
        )

        print(best_result)

        print("Training tunned model")
        modelo = Perceptron(
            max_iter=params["parameters"]["max_iter"],
            eta0=best_result["eta0"],
            random_state=42,
        )
        modelo.fit(X_train, y_train)
        f.close()

        # Saving Model
        with open("../artifacts/model.pkl", "wb") as file:
            pickle.dump(modelo, file)

    return {"model": modelo, "X_test": X_test, "y_test": y_test}


def evaluate_model(
    model: Perceptron, X_test: pd.DataFrame, y_test: pd.DataFrame
) -> str:
    """
    Function to evaluate the model.

    model:   sklearn model object.
    X_test:  A dataframe with testing features.
    y_test:  A dataframe with testing label.
    """

    # Predictions
    preds = model.predict(X_test)

    # Calculating metrics
    scores = {
        "acc": accuracy_score(y_test, preds),
        "recall": recall_score(y_test, preds, average="weighted"),
        "precision": precision_score(y_test, preds, average="weighted"),
        "f1": f1_score(y_test, preds, average="weighted"),
    }

    print(scores)

    # Loading label encoder
    with open("../artifacts/label_encoder.pkl", "rb") as le_file:
        LE = pickle.load(le_file)

    # Confusion_matrix
    labels = set(LE.inverse_transform(preds))
    confusion_matrix = metrics.confusion_matrix(y_test, preds)
    cm_display = metrics.ConfusionMatrixDisplay(confusion_matrix, display_labels=labels)

    cm_display.plot()
    plt.savefig("../artifacts/confusion_matrix.jpg")
    plt.show()

    # Saving scores
    with open("../artifacts/scores.csv", "w") as f:
        w = csv.writer(f)
        w.writerows(scores.items())

    return "Pipeline completed!!!"


if __name__ == "__main__":
    # Pipeline
    evaluate_model(**training(**transform(**split_data(**get_data()))))
