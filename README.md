# iris_classification_project

## Introdução.
Neste projeto construimos um modelo de machine learning que seja capaz de classificar as flores em uma das três espécies com base em suas características. 

## Como executar o Pipeline.

A execução do pipeline pode acontecer de duas formas. Na primeira forma qualquer alteração no project.yaml ou em qualquer arquivo .py dentro do diretorio src
irá triggar o pipeline dentro do contexto do CI-CD testando assim se esta tudo certo com o pipeline.

A segunda forma é localmente, dentro do diretorio src/ executar o comando abaixo:

```
python pipeline.py 
```
Recomendo uma execução via CI-CD para o teste do pipeline e uma execução local para tornar os artefatos disponíveis para a API.

## Como testar a API Localmente.

Para testar a API localmente executamos o comando abaixo dentro do diretorio app/, disponibilizando uma url localhost.

```
flask run

ou

python app.py
```

Com o servidor local radando, podemos executar o comando abaixo a partir de algum jupyternotebook.

```
import requests, json

url = 'http://127.0.0.1:5000/predict'
headers= {'content-type': 'application/json', 'Accept-Charset': 'UTF-8'}
data_load = json.dumps({"sepal length":[0.5],
                       "sepal width":[1],
                       "petal length":[2],
                       "petal width":[0.73]})

r = requests.post(url, data=data_load, headers=headers)
print(r, r.text)
```
Outro forma pode ser realizando a execução do script test_pipeline.py no diretorio src/.


